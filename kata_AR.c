/**
 * @file kata_AR.c
 * @brief Cálculo de dos pares AR (arábigo-romano)
 * @author Manuel Nava
 * @date 17/10/2022
 */

#include <stdio.h>
int calculoAR( int A1, char R1, int A2, char R2 );
int romanoAdecimal(char R);

//------- MAIN PROGRAM -------//
int main(){
    
    // Declaración de variables de entrada (A1-R1-A2-R2)
    int A1, A2;
    char R1, R2;
    
    // Entrada de datos
    printf("Ingrese pares arábigo-romano [ARAR]: ");
    scanf("%d%c%d%c",&A1,&R1,&A2,&R2);
    
    // Cálculo del valor de ambos pares AR
    int Resultado = calculoAR(A1, R1, A2, R2);
    
    // Salida de datos
    printf("\tResultado: %d", Resultado);
    return 0;
}

//------- Funcion de Calculo de Pares -------//
int calculoAR( int A1, char R1, int A2, char R2 ){
    int RAux1 = romanoAdecimal(R1);
    int RAux2 = romanoAdecimal(R2);
    int Resultado = 0;
    
    // CÁLCULO DE VALORES
    if( RAux1 >= RAux2 ){
        Resultado = (A1*RAux1) + (A2*RAux2);
    }
    else if( RAux1 < RAux2 ){
        Resultado = - (A1*RAux1) - (A2*RAux2);
    }
    
    return Resultado;
}

//------- Funcion de Conversión Romano a Decimal -------//
int romanoAdecimal(char R){
    switch(R){
        case 'M':
            return 1000;
        case 'D':
            return 500;
        case 'C':
            return 100;
        case 'L':
            return 50;
        case 'X':
            return 10;
        case 'V':
            return 5;
        case 'I':
            return 1;
        default:
            return 0;
    }
}